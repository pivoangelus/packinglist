//
//  AppDelegate.swift
//  packinglist
//
//  Created by Justin Andros on 4/6/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }

}

